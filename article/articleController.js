'use strict';
angular.module('articleModule', ['firebase'])
    .controller('ArticleController', ['$scope','$firebaseObject','$routeParams', function($scope,$firebaseObject,$routeParams){
//        var ref = new Firebase("https://viawestblog.firebaseio.com/articles");

        $scope.thisArticle = $routeParams.blogNumber;

        function resetComment(){
            $scope.SubmitClicked = false;
            $scope.newComment = {
                "name": "",
                "email": "",
                "comment": ""
            }
        }
        resetComment();


        var ref = new Firebase("https://viawestblog.firebaseio.com/articles/" +  $scope.thisArticle);
        $scope.article = $firebaseObject(ref);

        $scope.postComment = function(){
            $scope.SubmitClicked = true;
            if($scope.commentForm.$valid){
                var ref = new Firebase("https://viawestblog.firebaseio.com");
                var jobsRef = ref.child("comments");
                var newCommentRef = jobsRef.push().set({
                    blogNumber: $routeParams.blogNumber,
                    name: $scope.newComment.name,
                    email: $scope.newComment.email,
                    comment: $scope.newComment.comment
                });
                resetComment();
                //Todo would make this a toaster with more time.
                alert('comment saved')
            }
        }

    }])