'use strict';
angular.module('blogModule',['firebase'])
    .controller('BlogController', ['$scope','$firebaseArray', function($scope,$firebaseArray){
        var ref = new Firebase("https://viawestblog.firebaseio.com/articles");
        $scope.articles = $firebaseArray(ref);
    }])