'use strict';

var app = angular.module('viaBlogApp', [
    'articleModule',
    'blogModule',
    'commentsModule',
    'firebase',
    'ngRoute'])
    .config(['$routeProvider', function($routeProvider) {
        $routeProvider.when('/blog', {templateUrl: '../../blog/blog.html', controller: 'BlogController'});
        $routeProvider.when('/blog/:blogNumber', {templateUrl: '../../article/article.html', controller: 'ArticleController'});
        $routeProvider.otherwise({redirectTo: '/blog'});
    }]);